/**
 * @file      msp_i2c_master_raspbian.c
 * @author    John Wikman
 * @copyright MIT License
 * @brief     I2C interface for Raspbian.
 *
 * @details
 * Interface for I2C-1 on Raspbian. Neither the clockrate or timeout can be set
 * here. They must instead by set in the file /boot/config.txt or through some
 * other means in the operating system.
 *
 * Specifically, the following two changes should be set in /boot/config.txt:
 *  - dtparam=i2c_arm=on
 *  - dtparam=i2c_arm_baudrate=400000 (if a 400kHz clock is needed)
 */

#include <fcntl.h>
#include <stropts.h>
#include <stdlib.h>
#include <unistd.h>
#include <linux/i2c-dev.h>
#include <linux/i2c.h>

/* Error codes */
#define MSP_I2C_INITIALIZATION_ERROR -1
#define MSP_I2C_PARAMETER_ERROR      -2
#define MSP_I2C_ADDRESS_ERROR        -3
#define MSP_I2C_MEMORY_ERROR         -4
#define MSP_I2C_WRITE_ERROR           4
#define MSP_I2C_READ_ERROR            5


/* This states that the most significant bit will leak into the next byte */
#define RPI_I2C_MSB_BIT_LEAK

static int device_handle;
static int is_started = 0;
static unsigned long timeout_limit = 0;

/* Start the I2C driver */
int msp_i2c_start(unsigned long i2c_clockrate, unsigned long i2c_timeout)
{
	/* Do nothing if I2C is already started */
	if (is_started)
		return 0;

	/* I2C-1 should be accessible through /dev/i2c-1 */
	device_handle = open("/dev/i2c-1", O_RDWR);
	if (device_handle < 0) {
		/* Could not open I2C-1 */
		return MSP_I2C_INITIALIZATION_ERROR;
	}
	
	is_started = 1;
	timeout_limit = i2c_timeout;
	return 0;
}

/* Set I2C timeout */
void msp_i2c_set_timeout(unsigned long i2c_timeout)
{
	timeout_limit = i2c_timeout;
}

/* Stop the I2C driver */
void msp_i2c_stop(void)
{
	if (!is_started)
		return;

	close(device_handle);
	is_started = 0;
}

/* Write over I2C to the specified address */
int msp_i2c_write(unsigned long slave_address, unsigned char *data, unsigned long size)
{
	size_t bytes_written;

	if (!is_started)
		return MSP_I2C_INITIALIZATION_ERROR;
	if (slave_address > 0x7F)
		return MSP_I2C_PARAMETER_ERROR;
	if (data == NULL)
		return MSP_I2C_PARAMETER_ERROR;
	if (size < 1)
		return MSP_I2C_PARAMETER_ERROR;

	if (ioctl(device_handle, I2C_SLAVE, slave_address) < 0) {
		/* Could not set the slave address */
		return MSP_I2C_ADDRESS_ERROR;
	}

	bytes_written = write(device_handle, data, size);
	if (bytes_written != size) {
		/* Did not write the specified number of bytes */
		return MSP_I2C_WRITE_ERROR;
	}

	return 0; /* OK! */
}

/* Read over I2C from the specified address */
int msp_i2c_read(unsigned long slave_address, unsigned char *data, unsigned long size)
{
	size_t bytes_read;

	if (!is_started)
		return MSP_I2C_INITIALIZATION_ERROR;
	if (slave_address > 0x7F)
		return MSP_I2C_PARAMETER_ERROR;
	if (data == NULL)
		return MSP_I2C_PARAMETER_ERROR;
	if (size < 1)
		return MSP_I2C_PARAMETER_ERROR;

	if (ioctl(device_handle, I2C_SLAVE, slave_address) < 0) {
		/* Could not set the slave address */
		return MSP_I2C_ADDRESS_ERROR;
	}

#ifdef RPI_I2C_MSB_BIT_LEAK
	/* If the most significant bit in a byte leaks into the next byte, then we
	 * read an extra byte to make sure that we get the correct value of the
	 * last byte. Here we abuse the fact that MSP will pad with extra bytes if
	 * we try to read too much. */
	unsigned char *buf = malloc(sizeof(unsigned char) * (size + 1));
	if (buf == NULL)
		return MSP_I2C_MEMORY_ERROR;

	bytes_read = read(device_handle, buf, size + 1);
	if (bytes_read != (size + 1)) {
		/* Did not read the specified number of bytes */
		free(buf);
		return MSP_I2C_READ_ERROR;
	}

	/* Include the leaked bit for each byte */
	for (unsigned long i = 0; i < size; i++)
		data[i] = (buf[i] & 0x7F) | (buf[i+1] & 0x80);

	free(buf);

#else /* if no I2C MSP leak */
	bytes_read = read(device_handle, data, size);
	if (bytes_read != size) {
		/* Did not read the specified number of bytes */
		return MSP_I2C_READ_ERROR;
	}
#endif

	return 0; /* OK! */
}
