/**
 * @file      msp_configuration.h
 * @author    John Wikman
 * @copyright MIT License
 * @brief     Placeholder file for the configuration script.
 *
 * @details
 * This is a placeholder file that will get overwritten when running the
 * configuration script. All the configurable constants will be defined in this
 * file.
 */

#ifndef MSP_CONFIGURATION_H
#define MSP_CONFIGURATION_H

/* Include Doxygen documentation for constants */
#ifdef DOXYGEN_ONLY

/**
 * @brief The address of the experiment. (EXPERIMENT MODE ONLY)
 *
 * The value of this constant will be set by the configuration script. The
 * value set by the configuration script will be between 0x00 and 0x7F.
 */
#define MSP_EXP_ADDR 0x11 (placeholder value)

/**
 * @brief The MTU of the experiment. (EXPERIMENT MODE ONLY)
 *
 * The value of this constant will be set by the configuration script. The
 * value set by the configuration script will be an integer greater than or
 * equal to 4.
 */
#define MSP_EXP_MTU 507 (placeholder value)

/**
 * @brief Configures MSP to use less memory.
 *
 * If defined, then certain features will attempt to use less memory. For
 * example by manually computing constants instead of using lookup tables.
 */
#define MSP_LOW_MEMORY

#endif

#endif /* MSP_CONFIGURATION_H */
