/*
 * MSP Common Test 02
 * Author: John Wikman
 *
 * Tests integrity of the precomputed CRC table
 */

#include <stdio.h>

#include "test_common.h"

#include <msp/../../src/msp_crc.c>

#ifdef MSP_LOW_MEMORY
#error Cannot run this test when MSP is not configured to use CRC lookup table.
#endif

static unsigned long test_crc32[256];

/* Calculate CRC32 table. */
static void test_calculate_crc32()
{
	int i, j;
	unsigned long rem;
	for (i = 0; i < 256; i++) {
		rem = i;  /* remainder from polynomial division */
		for (j = 0; j < 8; j++) {
			if (rem & 1) {
				rem >>= 1;
				rem ^= MSP_CRC32_POLYNOMIAL;
			} else {
				rem >>= 1;
			}
		}
		test_crc32[i] = rem;
	}
}

void test(void)
{
	int i;
	char msg[128];
	test_calculate_crc32();

	for (i = 0; i < 256; i++) {
		sprintf(msg, "idx %d (msp_table_crc32[%d]=%08lX) (test_crc32[%d]=%08lX)", i, i, msp_table_crc32[i], i, test_crc32[i]);
		test_assert(msp_table_crc32[i] == test_crc32[i], msg);
	}

	return;
}
