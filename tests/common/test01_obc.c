/*
 * MSP Common Test 01
 * Author: John Wikman
 *
 * Tests OBC configuration with separate files
 */

#include <msp/msp_obc.h>

#include "test_common.h"

void test(void)
{
	/* Nothing to do here, just want to check that the tests runs */
	return;
}

int msp_i2c_write(unsigned long slave_address, unsigned char *data, unsigned long size)
{
	test_assert(0, "msp_i2c_write should be unreachable");
	return 0;
}
int msp_i2c_read(unsigned long slave_address, unsigned char *data, unsigned long size)
{
	test_assert(0, "msp_i2c_read should be unreachable");
	return 0;
}
