/*
 * MSP Experiment Test 18
 * Author: John Wikman
 */

#include "test_exp.h"

static int seq = 0;

void test(void)
{
	unsigned long i;
	static unsigned char buf[1000];
	unsigned long fcs;
	unsigned long len;
	int code;

	/* OBC Sending PUS (1000 bytes) */
	msp_seqflags_set(&msp_exp_state.seqflags, MSP_OP_SEND_PUS, 0);

	/* Format send frame */
	buf[0] = MSP_OP_SEND_PUS | 0x80;
	msp_to_bigendian32(buf+1, 1000);
	fcs = msp_exp_frame_generate_fcs(buf, 1, 5);
	msp_to_bigendian32(buf+5, fcs);
	code = msp_recv_callback(buf, 9);
	test_assert(code == 0, "Unexpected error (1)");
	DEBUG_EXEC(if (code) printf("code: %d\n", code));

	/* Get acknowledge header */
	code = msp_send_callback(buf, &len);
	test_assert(code == 0, "Unexpected error (2)");
	DEBUG_EXEC(if (code) printf("code: %d\n", code));
	test_assert(len == 9, "length of ack frame");
	test_assert((buf[0] & 0x7F) == MSP_OP_F_ACK, "opcode of ack frame");
	test_assert((buf[0] & 0x80) == 0x80, "frame-ID of ack frame");
	test_assert(msp_exp_frame_generate_fcs(buf, 0, 5) == msp_from_bigendian32(buf+5), "FCS calculation (1)");

	/* OBC sends first chunk of data (pretend that this gets corrupted) */
	buf[0] = MSP_OP_DATA_FRAME | 0x00;
	for (i = 1; i < 508; i++)
		buf[i] = 0x33;
	fcs = msp_exp_frame_generate_fcs(buf, 1, 508);
	msp_to_bigendian32(buf+508, fcs);
	buf[10] ^= 0x40; /* Corrupt the frame */
	code = msp_recv_callback(buf, 512);
	test_assert(code == MSP_EXP_ERR_FCS_MISMATCH, "Should get FCS mismatch");
	DEBUG_EXEC(if (code != MSP_EXP_ERR_FCS_MISMATCH) printf("code: %d\n", code));

	/* Should receive ACK for header frame */
	code = msp_send_callback(buf, &len);
	test_assert(code == 0, "Unexpected error (4)");
	DEBUG_EXEC(if (code) printf("code: %d\n", code));
	test_assert(len == 9, "length of ack frame");
	test_assert((buf[0] & 0x7F) == MSP_OP_F_ACK, "opcode of ack frame");
	test_assert((buf[0] & 0x80) == 0x80, "frame-ID of ack frame (should be duplicate)");
	test_assert(msp_exp_frame_generate_fcs(buf, 0, 5) == msp_from_bigendian32(buf+5), "FCS calculation (2)");

	/* OBC sends same chunk of data again */
	buf[0] = MSP_OP_DATA_FRAME | 0x00;
	for (i = 1; i < 508; i++)
		buf[i] = 0x33;
	fcs = msp_exp_frame_generate_fcs(buf, 1, 508);
	msp_to_bigendian32(buf+508, fcs);
	code = msp_recv_callback(buf, 512);
	test_assert(code == 0, "Unexpected error (5)");
	DEBUG_EXEC(if (code) printf("code: %d\n", code));

	/* Should receive ACK for data frame */
	code = msp_send_callback(buf, &len);
	test_assert(code == 0, "Unexpected error (6)");
	DEBUG_EXEC(if (code) printf("code: %d\n", code));
	test_assert(len == 9, "length of ack frame");
	test_assert((buf[0] & 0x7F) == MSP_OP_F_ACK, "opcode of ack frame");
	test_assert((buf[0] & 0x80) == 0x00, "frame-ID of ack frame");
	test_assert(msp_exp_frame_generate_fcs(buf, 0, 5) == msp_from_bigendian32(buf+5), "FCS calculation (3)");

	/* OBC sends last chunk of data */
	buf[0] = MSP_OP_DATA_FRAME | 0x80;
	for (i = 1; i < 508; i++)
		buf[i] = 0x71;
	fcs = msp_exp_frame_generate_fcs(buf, 1, (1000 - 507) + 1);
	msp_to_bigendian32(buf+(1000 - 507 + 1), fcs);
	code = msp_recv_callback(buf, 1000 - 507 + 1 + 4);
	test_assert(code == 0, "Unexpected error (7)");
	DEBUG_EXEC(if (code) printf("code: %d\n", code));

	/* Get transaction ack */
	code = msp_send_callback(buf, &len);
	test_assert(code == 0, "Unexpected error (8)");
	DEBUG_EXEC(if (code) printf("code: %d\n", code));
	test_assert(len == 9, "length of transaction ack");
	test_assert((buf[0] & 0x7F) == MSP_OP_T_ACK, "opcode of transaction ack");
	test_assert((buf[0] & 0x80) == 0x80, "frame-ID of transaction ack");
	test_assert(msp_exp_frame_generate_fcs(buf, 0, 5) == msp_from_bigendian32(buf+5), "FCS calculation (4)");

	test_assert(seq == 4, "should call 4 handlers");
	return;
}


void msp_exprecv_data(unsigned char opcode, const unsigned char *buf, unsigned long len, unsigned long offset)
{
	unsigned long i;
	int data_ok;

	switch (seq) {
	case 1:
		test_assert(len == 507, "first chunk of data should be 507 bytes in size");
		test_assert(offset == 0, "");
		data_ok = 1;
		for (i = 0; i < 507; i++) {
			if (buf[i] != 0x33)
				data_ok = 0;
		}
		test_assert(data_ok, "all data in first chunk should be 0x33");
		break;
	case 2:
		test_assert(len == (1000 - 507), "size of last chunk");
		test_assert(offset == 507, "");
		data_ok = 1;
		for (i = 0; i < (1000 - 507); i++) {
			if (buf[i] != 0x71)
				data_ok = 0;
		}
		test_assert(data_ok, "all data in second chunk should be 0x71");
		break;
	default:
		test_assert(0, "msp_exprecv_data called out of sequence");
		break;
	}

	seq++;
}
void msp_expsend_data(unsigned char opcode, unsigned char *buf, unsigned long len, unsigned long offset)
{
	test_assert(0, "msp_expsend_data should be unreachable");
}

void msp_exprecv_start(unsigned char opcode, unsigned long len)
{
	test_assert(seq == 0, "msp_exprecv_start should be called first");
	test_assert(opcode == MSP_OP_SEND_PUS, "start has correct opcode");
	test_assert(len == 1000, "SEND_PUS should have 1000 bytes of data");

	seq++;
}
void msp_expsend_start(unsigned char opcode, unsigned long *len)
{
	test_assert(0, "msp_expsend_start should be unreachable");
}

void msp_exprecv_complete(unsigned char opcode)
{
	test_assert(seq == 3, "msp_exprecv_complete should be called fourth");
	test_assert(opcode == MSP_OP_SEND_PUS, "opcode in complete");

	seq++;
}
void msp_expsend_complete(unsigned char opcode)
{
	test_assert(0, "msp_expsend_complete should be unreachable");
}

void msp_exprecv_error(unsigned char opcode, int error)
{
	test_assert(0, "msp_exprecv_error should be unreachable");
}
void msp_expsend_error(unsigned char opcode, int error)
{
	test_assert(0, "msp_expsend_error should be unreachable");
}

void msp_exprecv_syscommand(unsigned char opcode)
{
	test_assert(0, "msp_exprecv_syscommand should be unreachable");
}
